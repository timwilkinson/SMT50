SMT50
=====

The SMT50 is a pick-and-place machine available from http://hothotsmtmachine.com in China. The machine is quite cheap
(as such machines go) and also has a vision system, a significant advantage over other cheap chinese pnp machines. The
software that comes with the machine is also quite extensive and flexible, but the English translations are not the best.
Similarly, the video how-tos that come with the machines are all in Mandarin with no English subtitles or transcript.

This project is a repository of all the information I've gather or generated in order to make the SMT50 a little easier to
use and a little more turnkey. I hope other will find it useful to and it will encourage adoption of this excellent machine.
